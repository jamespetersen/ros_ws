# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "hv_v2v_messages: 3 messages, 0 services")

set(MSG_I_FLAGS "-Ihv_v2v_messages:/home/andy/catkin_ws/src/hv_v2v_messages/msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(hv_v2v_messages_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_angle.msg" NAME_WE)
add_custom_target(_hv_v2v_messages_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "hv_v2v_messages" "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_angle.msg" ""
)

get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/tractor_speed.msg" NAME_WE)
add_custom_target(_hv_v2v_messages_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "hv_v2v_messages" "/home/andy/catkin_ws/src/hv_v2v_messages/msg/tractor_speed.msg" ""
)

get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_length.msg" NAME_WE)
add_custom_target(_hv_v2v_messages_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "hv_v2v_messages" "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_length.msg" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_angle.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hv_v2v_messages
)
_generate_msg_cpp(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_length.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hv_v2v_messages
)
_generate_msg_cpp(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/tractor_speed.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hv_v2v_messages
)

### Generating Services

### Generating Module File
_generate_module_cpp(hv_v2v_messages
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hv_v2v_messages
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(hv_v2v_messages_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(hv_v2v_messages_generate_messages hv_v2v_messages_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_angle.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_cpp _hv_v2v_messages_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/tractor_speed.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_cpp _hv_v2v_messages_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_length.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_cpp _hv_v2v_messages_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(hv_v2v_messages_gencpp)
add_dependencies(hv_v2v_messages_gencpp hv_v2v_messages_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS hv_v2v_messages_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_angle.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hv_v2v_messages
)
_generate_msg_eus(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_length.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hv_v2v_messages
)
_generate_msg_eus(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/tractor_speed.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hv_v2v_messages
)

### Generating Services

### Generating Module File
_generate_module_eus(hv_v2v_messages
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hv_v2v_messages
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(hv_v2v_messages_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(hv_v2v_messages_generate_messages hv_v2v_messages_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_angle.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_eus _hv_v2v_messages_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/tractor_speed.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_eus _hv_v2v_messages_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_length.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_eus _hv_v2v_messages_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(hv_v2v_messages_geneus)
add_dependencies(hv_v2v_messages_geneus hv_v2v_messages_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS hv_v2v_messages_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_angle.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hv_v2v_messages
)
_generate_msg_lisp(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_length.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hv_v2v_messages
)
_generate_msg_lisp(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/tractor_speed.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hv_v2v_messages
)

### Generating Services

### Generating Module File
_generate_module_lisp(hv_v2v_messages
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hv_v2v_messages
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(hv_v2v_messages_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(hv_v2v_messages_generate_messages hv_v2v_messages_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_angle.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_lisp _hv_v2v_messages_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/tractor_speed.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_lisp _hv_v2v_messages_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_length.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_lisp _hv_v2v_messages_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(hv_v2v_messages_genlisp)
add_dependencies(hv_v2v_messages_genlisp hv_v2v_messages_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS hv_v2v_messages_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_angle.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hv_v2v_messages
)
_generate_msg_nodejs(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_length.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hv_v2v_messages
)
_generate_msg_nodejs(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/tractor_speed.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hv_v2v_messages
)

### Generating Services

### Generating Module File
_generate_module_nodejs(hv_v2v_messages
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hv_v2v_messages
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(hv_v2v_messages_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(hv_v2v_messages_generate_messages hv_v2v_messages_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_angle.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_nodejs _hv_v2v_messages_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/tractor_speed.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_nodejs _hv_v2v_messages_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_length.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_nodejs _hv_v2v_messages_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(hv_v2v_messages_gennodejs)
add_dependencies(hv_v2v_messages_gennodejs hv_v2v_messages_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS hv_v2v_messages_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_angle.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hv_v2v_messages
)
_generate_msg_py(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_length.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hv_v2v_messages
)
_generate_msg_py(hv_v2v_messages
  "/home/andy/catkin_ws/src/hv_v2v_messages/msg/tractor_speed.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hv_v2v_messages
)

### Generating Services

### Generating Module File
_generate_module_py(hv_v2v_messages
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hv_v2v_messages
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(hv_v2v_messages_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(hv_v2v_messages_generate_messages hv_v2v_messages_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_angle.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_py _hv_v2v_messages_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/tractor_speed.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_py _hv_v2v_messages_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/hv_v2v_messages/msg/trailer_length.msg" NAME_WE)
add_dependencies(hv_v2v_messages_generate_messages_py _hv_v2v_messages_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(hv_v2v_messages_genpy)
add_dependencies(hv_v2v_messages_genpy hv_v2v_messages_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS hv_v2v_messages_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hv_v2v_messages)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hv_v2v_messages
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
add_dependencies(hv_v2v_messages_generate_messages_cpp std_msgs_generate_messages_cpp)

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hv_v2v_messages)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hv_v2v_messages
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
add_dependencies(hv_v2v_messages_generate_messages_eus std_msgs_generate_messages_eus)

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hv_v2v_messages)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hv_v2v_messages
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
add_dependencies(hv_v2v_messages_generate_messages_lisp std_msgs_generate_messages_lisp)

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hv_v2v_messages)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hv_v2v_messages
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
add_dependencies(hv_v2v_messages_generate_messages_nodejs std_msgs_generate_messages_nodejs)

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hv_v2v_messages)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hv_v2v_messages\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hv_v2v_messages
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
add_dependencies(hv_v2v_messages_generate_messages_py std_msgs_generate_messages_py)
