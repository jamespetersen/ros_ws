# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "obe: 1 messages, 1 services")

set(MSG_I_FLAGS "-Iobe:/home/andy/catkin_ws/src/obe/msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(obe_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/andy/catkin_ws/src/obe/srv/AddTwoInts.srv" NAME_WE)
add_custom_target(_obe_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "obe" "/home/andy/catkin_ws/src/obe/srv/AddTwoInts.srv" ""
)

get_filename_component(_filename "/home/andy/catkin_ws/src/obe/msg/Num.msg" NAME_WE)
add_custom_target(_obe_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "obe" "/home/andy/catkin_ws/src/obe/msg/Num.msg" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(obe
  "/home/andy/catkin_ws/src/obe/msg/Num.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/obe
)

### Generating Services
_generate_srv_cpp(obe
  "/home/andy/catkin_ws/src/obe/srv/AddTwoInts.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/obe
)

### Generating Module File
_generate_module_cpp(obe
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/obe
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(obe_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(obe_generate_messages obe_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/andy/catkin_ws/src/obe/srv/AddTwoInts.srv" NAME_WE)
add_dependencies(obe_generate_messages_cpp _obe_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/obe/msg/Num.msg" NAME_WE)
add_dependencies(obe_generate_messages_cpp _obe_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(obe_gencpp)
add_dependencies(obe_gencpp obe_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS obe_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(obe
  "/home/andy/catkin_ws/src/obe/msg/Num.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/obe
)

### Generating Services
_generate_srv_eus(obe
  "/home/andy/catkin_ws/src/obe/srv/AddTwoInts.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/obe
)

### Generating Module File
_generate_module_eus(obe
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/obe
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(obe_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(obe_generate_messages obe_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/andy/catkin_ws/src/obe/srv/AddTwoInts.srv" NAME_WE)
add_dependencies(obe_generate_messages_eus _obe_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/obe/msg/Num.msg" NAME_WE)
add_dependencies(obe_generate_messages_eus _obe_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(obe_geneus)
add_dependencies(obe_geneus obe_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS obe_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(obe
  "/home/andy/catkin_ws/src/obe/msg/Num.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/obe
)

### Generating Services
_generate_srv_lisp(obe
  "/home/andy/catkin_ws/src/obe/srv/AddTwoInts.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/obe
)

### Generating Module File
_generate_module_lisp(obe
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/obe
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(obe_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(obe_generate_messages obe_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/andy/catkin_ws/src/obe/srv/AddTwoInts.srv" NAME_WE)
add_dependencies(obe_generate_messages_lisp _obe_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/obe/msg/Num.msg" NAME_WE)
add_dependencies(obe_generate_messages_lisp _obe_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(obe_genlisp)
add_dependencies(obe_genlisp obe_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS obe_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(obe
  "/home/andy/catkin_ws/src/obe/msg/Num.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/obe
)

### Generating Services
_generate_srv_nodejs(obe
  "/home/andy/catkin_ws/src/obe/srv/AddTwoInts.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/obe
)

### Generating Module File
_generate_module_nodejs(obe
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/obe
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(obe_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(obe_generate_messages obe_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/andy/catkin_ws/src/obe/srv/AddTwoInts.srv" NAME_WE)
add_dependencies(obe_generate_messages_nodejs _obe_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/obe/msg/Num.msg" NAME_WE)
add_dependencies(obe_generate_messages_nodejs _obe_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(obe_gennodejs)
add_dependencies(obe_gennodejs obe_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS obe_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(obe
  "/home/andy/catkin_ws/src/obe/msg/Num.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/obe
)

### Generating Services
_generate_srv_py(obe
  "/home/andy/catkin_ws/src/obe/srv/AddTwoInts.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/obe
)

### Generating Module File
_generate_module_py(obe
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/obe
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(obe_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(obe_generate_messages obe_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/andy/catkin_ws/src/obe/srv/AddTwoInts.srv" NAME_WE)
add_dependencies(obe_generate_messages_py _obe_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/andy/catkin_ws/src/obe/msg/Num.msg" NAME_WE)
add_dependencies(obe_generate_messages_py _obe_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(obe_genpy)
add_dependencies(obe_genpy obe_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS obe_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/obe)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/obe
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
add_dependencies(obe_generate_messages_cpp std_msgs_generate_messages_cpp)

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/obe)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/obe
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
add_dependencies(obe_generate_messages_eus std_msgs_generate_messages_eus)

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/obe)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/obe
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
add_dependencies(obe_generate_messages_lisp std_msgs_generate_messages_lisp)

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/obe)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/obe
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
add_dependencies(obe_generate_messages_nodejs std_msgs_generate_messages_nodejs)

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/obe)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/obe\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/obe
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
add_dependencies(obe_generate_messages_py std_msgs_generate_messages_py)
