set(_CATKIN_CURRENT_PACKAGE "obe")
set(obe_VERSION "0.0.0")
set(obe_MAINTAINER "andy <andy@todo.todo>")
set(obe_PACKAGE_FORMAT "1")
set(obe_BUILD_DEPENDS "message_generation" "roscpp" "rospy" "std_msgs")
set(obe_BUILD_EXPORT_DEPENDS "message_runtime" "roscpp" "rospy" "std_msgs")
set(obe_BUILDTOOL_DEPENDS "catkin")
set(obe_BUILDTOOL_EXPORT_DEPENDS )
set(obe_EXEC_DEPENDS "message_runtime" "roscpp" "rospy" "std_msgs")
set(obe_RUN_DEPENDS "message_runtime" "roscpp" "rospy" "std_msgs")
set(obe_TEST_DEPENDS )
set(obe_DOC_DEPENDS )
set(obe_DEPRECATED "")