# CMake generated Testfile for 
# Source directory: /home/andy/catkin_ws/src
# Build directory: /home/andy/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(j1939)
subdirs(obe)
subdirs(spoof)
