# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(WARNING "Invoking generate_messages() without having added any message or service file before.
You should either add add_message_files() and/or add_service_files() calls or remove the invocation of generate_messages().")
message(STATUS "spoof: 0 messages, 0 services")

set(MSG_I_FLAGS "-Ispoof:/home/andy/catkin_ws/src/spoof/msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(spoof_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages

### Generating Services

### Generating Module File
_generate_module_cpp(spoof
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/spoof
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(spoof_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(spoof_generate_messages spoof_generate_messages_cpp)

# add dependencies to all check dependencies targets

# target for backward compatibility
add_custom_target(spoof_gencpp)
add_dependencies(spoof_gencpp spoof_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS spoof_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages

### Generating Services

### Generating Module File
_generate_module_eus(spoof
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/spoof
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(spoof_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(spoof_generate_messages spoof_generate_messages_eus)

# add dependencies to all check dependencies targets

# target for backward compatibility
add_custom_target(spoof_geneus)
add_dependencies(spoof_geneus spoof_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS spoof_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages

### Generating Services

### Generating Module File
_generate_module_lisp(spoof
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/spoof
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(spoof_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(spoof_generate_messages spoof_generate_messages_lisp)

# add dependencies to all check dependencies targets

# target for backward compatibility
add_custom_target(spoof_genlisp)
add_dependencies(spoof_genlisp spoof_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS spoof_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages

### Generating Services

### Generating Module File
_generate_module_nodejs(spoof
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/spoof
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(spoof_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(spoof_generate_messages spoof_generate_messages_nodejs)

# add dependencies to all check dependencies targets

# target for backward compatibility
add_custom_target(spoof_gennodejs)
add_dependencies(spoof_gennodejs spoof_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS spoof_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages

### Generating Services

### Generating Module File
_generate_module_py(spoof
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/spoof
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(spoof_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(spoof_generate_messages spoof_generate_messages_py)

# add dependencies to all check dependencies targets

# target for backward compatibility
add_custom_target(spoof_genpy)
add_dependencies(spoof_genpy spoof_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS spoof_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/spoof)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/spoof
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
add_dependencies(spoof_generate_messages_cpp std_msgs_generate_messages_cpp)

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/spoof)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/spoof
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
add_dependencies(spoof_generate_messages_eus std_msgs_generate_messages_eus)

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/spoof)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/spoof
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
add_dependencies(spoof_generate_messages_lisp std_msgs_generate_messages_lisp)

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/spoof)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/spoof
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
add_dependencies(spoof_generate_messages_nodejs std_msgs_generate_messages_nodejs)

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/spoof)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/spoof\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/spoof
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
add_dependencies(spoof_generate_messages_py std_msgs_generate_messages_py)
