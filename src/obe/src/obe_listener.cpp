#include "ros/ros.h"
#include "std_msgs/Float32.h"


// Module consumes these topics
// trailer_length :: Float32 (meters)
// trailer_angle :: Float32 (radians (need to figure out coordinate system))
// tractor_speed :: Float32 (meters_per_second)
// tractor_imu :: twist ()


void lengthCallback(const std_msgs::Float32::ConstPtr& msg)
{
  ROS_INFO("length: [%f]", msg->data);
}


void angleCallback(const std_msgs::Float32::ConstPtr& msg)
{
  ROS_INFO("angle: [%f]", msg->data);
}


void speedCallback(const std_msgs::Float32::ConstPtr& msg)
{
  ROS_INFO("speed: [%f]", msg->data);
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "obe");

  ros::NodeHandle n1;
  ros::Subscriber sub1 = n1.subscribe("trailer_length", 1000, lengthCallback);

  ros::NodeHandle n2;
  ros::Subscriber sub2 = n2.subscribe("trailer_angle", 1000, angleCallback);

  ros::NodeHandle n3;
  ros::Subscriber sub3 = n3.subscribe("tractor_speed", 1000, speedCallback);

  ros::spin();

  return 0;
}
