#include "ros/ros.h"
#include "std_msgs/Float32.h"



int main(int argc, char **argv)
{
  ros::init(argc, argv, "J1939");

  ros::NodeHandle n1;
  ros::Publisher pub1 = n1.advertise<std_msgs::Float32>("tractor_speed", 1000, true);

  //ros::Rate loop_rate(10);
  ros::Rate loop_rate(2);

  float tractor_speed=0.0;

  while (ros::ok())
  {
    std_msgs::Float32 msg_tractor_speed;

    msg_tractor_speed.data = tractor_speed;
    tractor_speed += 0.05;
    ROS_INFO("tractor_speed = %f", msg_tractor_speed.data);
    pub1.publish(msg_tractor_speed);

    ros::spinOnce();

    loop_rate.sleep();
  }


  return 0;
}
