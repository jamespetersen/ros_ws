#include "spoof/dynamic_talker.h"
#include "std_msgs/Float32.h"

namespace spoof
{

ExampleTalker::ExampleTalker(ros::NodeHandle nh) :
  trailer_length_(3), trailer_angle_(4), tractor_speed_(5)
{
  // Set up a dynamic reconfigure server.
  // Do this before parameter server, else some of the parameter server values can be overwritten.
  dynamic_reconfigure::Server<spoof::nodeExampleConfig>::CallbackType cb;
  cb = boost::bind(&ExampleTalker::configCallback, this, _1, _2);
  dr_srv_.setCallback(cb);

  // Declare variables that can be modified by launch file or command line.
  int rate;

  // Initialize node parameters from launch file or command line. Use a private node handle so that multiple instances
  // of the node can be run simultaneously while using different parameters.
  ros::NodeHandle pnh("~");
  pnh.param("trailer_length", trailer_length_, trailer_length_);
  pnh.param("trailer_angle", trailer_angle_, trailer_angle_);
  pnh.param("tractor_speed", tractor_speed_, tractor_speed_);
  pnh.param("rate", rate, 1);

  // Create a publisher and name the topic.
  pub1 = nh.advertise<std_msgs::Float32>("trailer_length", 10);
  pub2 = nh.advertise<std_msgs::Float32>("trailer_angle", 10);
  pub3 = nh.advertise<std_msgs::Float32>("tractor_speed", 10);

  // Create timer.
  timer_ = nh.createTimer(ros::Duration(1 / rate), &ExampleTalker::timerCallback, this);
}

void ExampleTalker::timerCallback(const ros::TimerEvent& event)
{
  std_msgs::Float32 msg_trailer_length;
  msg_trailer_length.data = trailer_length_;
  pub1.publish(msg_trailer_length);

  std_msgs::Float32 msg_trailer_angle;
  msg_trailer_angle.data = trailer_angle_;
  pub2.publish(msg_trailer_angle);

  std_msgs::Float32 msg_tractor_speed;
  msg_tractor_speed.data = tractor_speed_;
  pub3.publish(msg_tractor_speed);
}

void ExampleTalker::configCallback(spoof::nodeExampleConfig& config, uint32_t level)
{
  // Set class variables to new values. They should match what is input at the dynamic reconfigure GUI.
  trailer_length_ = config.trailer_length;
  trailer_angle_ = config.trailer_angle;
  tractor_speed_ = config.tractor_speed;
}

}
