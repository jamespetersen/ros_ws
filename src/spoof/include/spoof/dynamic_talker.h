#ifndef SPOOF_TALKER_H
#define SPOOF_TALKER_H

// ROS includes.
#include "ros/ros.h"
#include "ros/time.h"

// Dynamic reconfigure includes.
#include <dynamic_reconfigure/server.h>
// Auto-generated from cfg/ directory.
#include <spoof/nodeExampleConfig.h>

namespace spoof
{

class ExampleTalker
{
public:
  //! Constructor.
  ExampleTalker(ros::NodeHandle nh);

  //! Callback function for dynamic reconfigure server.
  void configCallback(spoof::nodeExampleConfig& config, uint32_t level);

  //! Timer callback for publishing message.
  void timerCallback(const ros::TimerEvent& event);

private:
  //! The timer variable used to go to callback function at specified rate.
  ros::Timer timer_;

  //! Message publisher.
  ros::Publisher pub1;
  ros::Publisher pub2;
  ros::Publisher pub3;

  //! Dynamic reconfigure server.
  dynamic_reconfigure::Server<spoof::nodeExampleConfig> dr_srv_;

  //! The actual message.
  double trailer_length_;
  double trailer_angle_;
  double tractor_speed_;
};

}

#endif // SPOOF_TALKER_H
