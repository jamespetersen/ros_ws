// Generated by gencpp from file spoof/NodeExampleData.msg
// DO NOT EDIT!


#ifndef SPOOF_MESSAGE_NODEEXAMPLEDATA_H
#define SPOOF_MESSAGE_NODEEXAMPLEDATA_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace spoof
{
template <class ContainerAllocator>
struct NodeExampleData_
{
  typedef NodeExampleData_<ContainerAllocator> Type;

  NodeExampleData_()
    : trailer_length(0.0)
    , trailer_angle(0.0)
    , tractor_speed(0.0)  {
    }
  NodeExampleData_(const ContainerAllocator& _alloc)
    : trailer_length(0.0)
    , trailer_angle(0.0)
    , tractor_speed(0.0)  {
  (void)_alloc;
    }



   typedef float _trailer_length_type;
  _trailer_length_type trailer_length;

   typedef float _trailer_angle_type;
  _trailer_angle_type trailer_angle;

   typedef float _tractor_speed_type;
  _tractor_speed_type tractor_speed;




  typedef boost::shared_ptr< ::spoof::NodeExampleData_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::spoof::NodeExampleData_<ContainerAllocator> const> ConstPtr;

}; // struct NodeExampleData_

typedef ::spoof::NodeExampleData_<std::allocator<void> > NodeExampleData;

typedef boost::shared_ptr< ::spoof::NodeExampleData > NodeExampleDataPtr;
typedef boost::shared_ptr< ::spoof::NodeExampleData const> NodeExampleDataConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::spoof::NodeExampleData_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::spoof::NodeExampleData_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace spoof

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'spoof': ['/home/andy/catkin_ws/src/spoof/msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::spoof::NodeExampleData_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::spoof::NodeExampleData_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::spoof::NodeExampleData_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::spoof::NodeExampleData_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::spoof::NodeExampleData_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::spoof::NodeExampleData_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::spoof::NodeExampleData_<ContainerAllocator> >
{
  static const char* value()
  {
    return "7c755488a7cae0c2d3e46552b9fab888";
  }

  static const char* value(const ::spoof::NodeExampleData_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x7c755488a7cae0c2ULL;
  static const uint64_t static_value2 = 0xd3e46552b9fab888ULL;
};

template<class ContainerAllocator>
struct DataType< ::spoof::NodeExampleData_<ContainerAllocator> >
{
  static const char* value()
  {
    return "spoof/NodeExampleData";
  }

  static const char* value(const ::spoof::NodeExampleData_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::spoof::NodeExampleData_<ContainerAllocator> >
{
  static const char* value()
  {
    return "float32 trailer_length\n\
float32 trailer_angle\n\
float32 tractor_speed\n\
";
  }

  static const char* value(const ::spoof::NodeExampleData_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::spoof::NodeExampleData_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.trailer_length);
      stream.next(m.trailer_angle);
      stream.next(m.tractor_speed);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER;
  }; // struct NodeExampleData_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::spoof::NodeExampleData_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::spoof::NodeExampleData_<ContainerAllocator>& v)
  {
    s << indent << "trailer_length: ";
    Printer<float>::stream(s, indent + "  ", v.trailer_length);
    s << indent << "trailer_angle: ";
    Printer<float>::stream(s, indent + "  ", v.trailer_angle);
    s << indent << "tractor_speed: ";
    Printer<float>::stream(s, indent + "  ", v.tractor_speed);
  }
};

} // namespace message_operations
} // namespace ros

#endif // SPOOF_MESSAGE_NODEEXAMPLEDATA_H
