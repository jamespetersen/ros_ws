// Generated by gencpp from file hv_v2v/AddTwoInts.msg
// DO NOT EDIT!


#ifndef HV_V2V_MESSAGE_ADDTWOINTS_H
#define HV_V2V_MESSAGE_ADDTWOINTS_H

#include <ros/service_traits.h>


#include <hv_v2v/AddTwoIntsRequest.h>
#include <hv_v2v/AddTwoIntsResponse.h>


namespace hv_v2v
{

struct AddTwoInts
{

typedef AddTwoIntsRequest Request;
typedef AddTwoIntsResponse Response;
Request request;
Response response;

typedef Request RequestType;
typedef Response ResponseType;

}; // struct AddTwoInts
} // namespace hv_v2v


namespace ros
{
namespace service_traits
{


template<>
struct MD5Sum< ::hv_v2v::AddTwoInts > {
  static const char* value()
  {
    return "6a2e34150c00229791cc89ff309fff21";
  }

  static const char* value(const ::hv_v2v::AddTwoInts&) { return value(); }
};

template<>
struct DataType< ::hv_v2v::AddTwoInts > {
  static const char* value()
  {
    return "hv_v2v/AddTwoInts";
  }

  static const char* value(const ::hv_v2v::AddTwoInts&) { return value(); }
};


// service_traits::MD5Sum< ::hv_v2v::AddTwoIntsRequest> should match 
// service_traits::MD5Sum< ::hv_v2v::AddTwoInts > 
template<>
struct MD5Sum< ::hv_v2v::AddTwoIntsRequest>
{
  static const char* value()
  {
    return MD5Sum< ::hv_v2v::AddTwoInts >::value();
  }
  static const char* value(const ::hv_v2v::AddTwoIntsRequest&)
  {
    return value();
  }
};

// service_traits::DataType< ::hv_v2v::AddTwoIntsRequest> should match 
// service_traits::DataType< ::hv_v2v::AddTwoInts > 
template<>
struct DataType< ::hv_v2v::AddTwoIntsRequest>
{
  static const char* value()
  {
    return DataType< ::hv_v2v::AddTwoInts >::value();
  }
  static const char* value(const ::hv_v2v::AddTwoIntsRequest&)
  {
    return value();
  }
};

// service_traits::MD5Sum< ::hv_v2v::AddTwoIntsResponse> should match 
// service_traits::MD5Sum< ::hv_v2v::AddTwoInts > 
template<>
struct MD5Sum< ::hv_v2v::AddTwoIntsResponse>
{
  static const char* value()
  {
    return MD5Sum< ::hv_v2v::AddTwoInts >::value();
  }
  static const char* value(const ::hv_v2v::AddTwoIntsResponse&)
  {
    return value();
  }
};

// service_traits::DataType< ::hv_v2v::AddTwoIntsResponse> should match 
// service_traits::DataType< ::hv_v2v::AddTwoInts > 
template<>
struct DataType< ::hv_v2v::AddTwoIntsResponse>
{
  static const char* value()
  {
    return DataType< ::hv_v2v::AddTwoInts >::value();
  }
  static const char* value(const ::hv_v2v::AddTwoIntsResponse&)
  {
    return value();
  }
};

} // namespace service_traits
} // namespace ros

#endif // HV_V2V_MESSAGE_ADDTWOINTS_H
