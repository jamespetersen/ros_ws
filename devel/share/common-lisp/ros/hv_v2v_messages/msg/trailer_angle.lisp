; Auto-generated. Do not edit!


(cl:in-package hv_v2v_messages-msg)


;//! \htmlinclude trailer_angle.msg.html

(cl:defclass <trailer_angle> (roslisp-msg-protocol:ros-message)
  ((angle
    :reader angle
    :initarg :angle
    :type cl:float
    :initform 0.0))
)

(cl:defclass trailer_angle (<trailer_angle>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <trailer_angle>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'trailer_angle)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name hv_v2v_messages-msg:<trailer_angle> is deprecated: use hv_v2v_messages-msg:trailer_angle instead.")))

(cl:ensure-generic-function 'angle-val :lambda-list '(m))
(cl:defmethod angle-val ((m <trailer_angle>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hv_v2v_messages-msg:angle-val is deprecated.  Use hv_v2v_messages-msg:angle instead.")
  (angle m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <trailer_angle>) ostream)
  "Serializes a message object of type '<trailer_angle>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'angle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <trailer_angle>) istream)
  "Deserializes a message object of type '<trailer_angle>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'angle) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<trailer_angle>)))
  "Returns string type for a message object of type '<trailer_angle>"
  "hv_v2v_messages/trailer_angle")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'trailer_angle)))
  "Returns string type for a message object of type 'trailer_angle"
  "hv_v2v_messages/trailer_angle")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<trailer_angle>)))
  "Returns md5sum for a message object of type '<trailer_angle>"
  "2d11dcdbe5a6f73dd324353dc52315ab")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'trailer_angle)))
  "Returns md5sum for a message object of type 'trailer_angle"
  "2d11dcdbe5a6f73dd324353dc52315ab")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<trailer_angle>)))
  "Returns full string definition for message of type '<trailer_angle>"
  (cl:format cl:nil "float32 angle~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'trailer_angle)))
  "Returns full string definition for message of type 'trailer_angle"
  (cl:format cl:nil "float32 angle~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <trailer_angle>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <trailer_angle>))
  "Converts a ROS message object to a list"
  (cl:list 'trailer_angle
    (cl:cons ':angle (angle msg))
))
