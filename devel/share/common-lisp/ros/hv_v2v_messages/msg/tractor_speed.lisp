; Auto-generated. Do not edit!


(cl:in-package hv_v2v_messages-msg)


;//! \htmlinclude tractor_speed.msg.html

(cl:defclass <tractor_speed> (roslisp-msg-protocol:ros-message)
  ((speed
    :reader speed
    :initarg :speed
    :type cl:float
    :initform 0.0))
)

(cl:defclass tractor_speed (<tractor_speed>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <tractor_speed>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'tractor_speed)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name hv_v2v_messages-msg:<tractor_speed> is deprecated: use hv_v2v_messages-msg:tractor_speed instead.")))

(cl:ensure-generic-function 'speed-val :lambda-list '(m))
(cl:defmethod speed-val ((m <tractor_speed>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hv_v2v_messages-msg:speed-val is deprecated.  Use hv_v2v_messages-msg:speed instead.")
  (speed m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <tractor_speed>) ostream)
  "Serializes a message object of type '<tractor_speed>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'speed))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <tractor_speed>) istream)
  "Deserializes a message object of type '<tractor_speed>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'speed) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<tractor_speed>)))
  "Returns string type for a message object of type '<tractor_speed>"
  "hv_v2v_messages/tractor_speed")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'tractor_speed)))
  "Returns string type for a message object of type 'tractor_speed"
  "hv_v2v_messages/tractor_speed")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<tractor_speed>)))
  "Returns md5sum for a message object of type '<tractor_speed>"
  "ca65bba734a79b4a6707341d829f4d5c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'tractor_speed)))
  "Returns md5sum for a message object of type 'tractor_speed"
  "ca65bba734a79b4a6707341d829f4d5c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<tractor_speed>)))
  "Returns full string definition for message of type '<tractor_speed>"
  (cl:format cl:nil "float32 speed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'tractor_speed)))
  "Returns full string definition for message of type 'tractor_speed"
  (cl:format cl:nil "float32 speed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <tractor_speed>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <tractor_speed>))
  "Converts a ROS message object to a list"
  (cl:list 'tractor_speed
    (cl:cons ':speed (speed msg))
))
