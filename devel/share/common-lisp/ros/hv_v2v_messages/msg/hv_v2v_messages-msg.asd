
(cl:in-package :asdf)

(defsystem "hv_v2v_messages-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "tractor_speed" :depends-on ("_package_tractor_speed"))
    (:file "_package_tractor_speed" :depends-on ("_package"))
    (:file "trailer_angle" :depends-on ("_package_trailer_angle"))
    (:file "_package_trailer_angle" :depends-on ("_package"))
    (:file "trailer_length" :depends-on ("_package_trailer_length"))
    (:file "_package_trailer_length" :depends-on ("_package"))
  ))