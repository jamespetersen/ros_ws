; Auto-generated. Do not edit!


(cl:in-package spoof-msg)


;//! \htmlinclude NodeExampleData.msg.html

(cl:defclass <NodeExampleData> (roslisp-msg-protocol:ros-message)
  ((trailer_length
    :reader trailer_length
    :initarg :trailer_length
    :type cl:float
    :initform 0.0)
   (trailer_angle
    :reader trailer_angle
    :initarg :trailer_angle
    :type cl:float
    :initform 0.0)
   (tractor_speed
    :reader tractor_speed
    :initarg :tractor_speed
    :type cl:float
    :initform 0.0))
)

(cl:defclass NodeExampleData (<NodeExampleData>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <NodeExampleData>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'NodeExampleData)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name spoof-msg:<NodeExampleData> is deprecated: use spoof-msg:NodeExampleData instead.")))

(cl:ensure-generic-function 'trailer_length-val :lambda-list '(m))
(cl:defmethod trailer_length-val ((m <NodeExampleData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader spoof-msg:trailer_length-val is deprecated.  Use spoof-msg:trailer_length instead.")
  (trailer_length m))

(cl:ensure-generic-function 'trailer_angle-val :lambda-list '(m))
(cl:defmethod trailer_angle-val ((m <NodeExampleData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader spoof-msg:trailer_angle-val is deprecated.  Use spoof-msg:trailer_angle instead.")
  (trailer_angle m))

(cl:ensure-generic-function 'tractor_speed-val :lambda-list '(m))
(cl:defmethod tractor_speed-val ((m <NodeExampleData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader spoof-msg:tractor_speed-val is deprecated.  Use spoof-msg:tractor_speed instead.")
  (tractor_speed m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <NodeExampleData>) ostream)
  "Serializes a message object of type '<NodeExampleData>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'trailer_length))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'trailer_angle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'tractor_speed))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <NodeExampleData>) istream)
  "Deserializes a message object of type '<NodeExampleData>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'trailer_length) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'trailer_angle) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'tractor_speed) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<NodeExampleData>)))
  "Returns string type for a message object of type '<NodeExampleData>"
  "spoof/NodeExampleData")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'NodeExampleData)))
  "Returns string type for a message object of type 'NodeExampleData"
  "spoof/NodeExampleData")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<NodeExampleData>)))
  "Returns md5sum for a message object of type '<NodeExampleData>"
  "7c755488a7cae0c2d3e46552b9fab888")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'NodeExampleData)))
  "Returns md5sum for a message object of type 'NodeExampleData"
  "7c755488a7cae0c2d3e46552b9fab888")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<NodeExampleData>)))
  "Returns full string definition for message of type '<NodeExampleData>"
  (cl:format cl:nil "float32 trailer_length~%float32 trailer_angle~%float32 tractor_speed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'NodeExampleData)))
  "Returns full string definition for message of type 'NodeExampleData"
  (cl:format cl:nil "float32 trailer_length~%float32 trailer_angle~%float32 tractor_speed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <NodeExampleData>))
  (cl:+ 0
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <NodeExampleData>))
  "Converts a ROS message object to a list"
  (cl:list 'NodeExampleData
    (cl:cons ':trailer_length (trailer_length msg))
    (cl:cons ':trailer_angle (trailer_angle msg))
    (cl:cons ':tractor_speed (tractor_speed msg))
))
