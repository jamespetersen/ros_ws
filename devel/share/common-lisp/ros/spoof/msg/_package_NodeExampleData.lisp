(cl:in-package spoof-msg)
(cl:export '(TRAILER_LENGTH-VAL
          TRAILER_LENGTH
          TRAILER_ANGLE-VAL
          TRAILER_ANGLE
          TRACTOR_SPEED-VAL
          TRACTOR_SPEED
))