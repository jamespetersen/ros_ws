; Auto-generated. Do not edit!


(cl:in-package obe-msg)


;//! \htmlinclude trailer_length.msg.html

(cl:defclass <trailer_length> (roslisp-msg-protocol:ros-message)
  ((length
    :reader length
    :initarg :length
    :type cl:float
    :initform 0.0))
)

(cl:defclass trailer_length (<trailer_length>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <trailer_length>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'trailer_length)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name obe-msg:<trailer_length> is deprecated: use obe-msg:trailer_length instead.")))

(cl:ensure-generic-function 'length-val :lambda-list '(m))
(cl:defmethod length-val ((m <trailer_length>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader obe-msg:length-val is deprecated.  Use obe-msg:length instead.")
  (length m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <trailer_length>) ostream)
  "Serializes a message object of type '<trailer_length>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'length))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <trailer_length>) istream)
  "Deserializes a message object of type '<trailer_length>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'length) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<trailer_length>)))
  "Returns string type for a message object of type '<trailer_length>"
  "obe/trailer_length")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'trailer_length)))
  "Returns string type for a message object of type 'trailer_length"
  "obe/trailer_length")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<trailer_length>)))
  "Returns md5sum for a message object of type '<trailer_length>"
  "30516591bd33c945cbc4d8d225f8022b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'trailer_length)))
  "Returns md5sum for a message object of type 'trailer_length"
  "30516591bd33c945cbc4d8d225f8022b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<trailer_length>)))
  "Returns full string definition for message of type '<trailer_length>"
  (cl:format cl:nil "float32 length~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'trailer_length)))
  "Returns full string definition for message of type 'trailer_length"
  (cl:format cl:nil "float32 length~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <trailer_length>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <trailer_length>))
  "Converts a ROS message object to a list"
  (cl:list 'trailer_length
    (cl:cons ':length (length msg))
))
