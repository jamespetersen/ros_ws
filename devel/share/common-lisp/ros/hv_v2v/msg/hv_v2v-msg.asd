
(cl:in-package :asdf)

(defsystem "hv_v2v-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Num" :depends-on ("_package_Num"))
    (:file "_package_Num" :depends-on ("_package"))
    (:file "trailer_angle" :depends-on ("_package_trailer_angle"))
    (:file "_package_trailer_angle" :depends-on ("_package"))
  ))