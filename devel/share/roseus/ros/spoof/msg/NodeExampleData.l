;; Auto-generated. Do not edit!


(when (boundp 'spoof::NodeExampleData)
  (if (not (find-package "SPOOF"))
    (make-package "SPOOF"))
  (shadow 'NodeExampleData (find-package "SPOOF")))
(unless (find-package "SPOOF::NODEEXAMPLEDATA")
  (make-package "SPOOF::NODEEXAMPLEDATA"))

(in-package "ROS")
;;//! \htmlinclude NodeExampleData.msg.html


(defclass spoof::NodeExampleData
  :super ros::object
  :slots (_trailer_length _trailer_angle _tractor_speed ))

(defmethod spoof::NodeExampleData
  (:init
   (&key
    ((:trailer_length __trailer_length) 0.0)
    ((:trailer_angle __trailer_angle) 0.0)
    ((:tractor_speed __tractor_speed) 0.0)
    )
   (send-super :init)
   (setq _trailer_length (float __trailer_length))
   (setq _trailer_angle (float __trailer_angle))
   (setq _tractor_speed (float __tractor_speed))
   self)
  (:trailer_length
   (&optional __trailer_length)
   (if __trailer_length (setq _trailer_length __trailer_length)) _trailer_length)
  (:trailer_angle
   (&optional __trailer_angle)
   (if __trailer_angle (setq _trailer_angle __trailer_angle)) _trailer_angle)
  (:tractor_speed
   (&optional __tractor_speed)
   (if __tractor_speed (setq _tractor_speed __tractor_speed)) _tractor_speed)
  (:serialization-length
   ()
   (+
    ;; float32 _trailer_length
    4
    ;; float32 _trailer_angle
    4
    ;; float32 _tractor_speed
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _trailer_length
       (sys::poke _trailer_length (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _trailer_angle
       (sys::poke _trailer_angle (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _tractor_speed
       (sys::poke _tractor_speed (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _trailer_length
     (setq _trailer_length (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _trailer_angle
     (setq _trailer_angle (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _tractor_speed
     (setq _tractor_speed (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get spoof::NodeExampleData :md5sum-) "7c755488a7cae0c2d3e46552b9fab888")
(setf (get spoof::NodeExampleData :datatype-) "spoof/NodeExampleData")
(setf (get spoof::NodeExampleData :definition-)
      "float32 trailer_length
float32 trailer_angle
float32 tractor_speed

")



(provide :spoof/NodeExampleData "7c755488a7cae0c2d3e46552b9fab888")


