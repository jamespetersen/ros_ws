;; Auto-generated. Do not edit!


(when (boundp 'hv_v2v::trailer_angle)
  (if (not (find-package "HV_V2V"))
    (make-package "HV_V2V"))
  (shadow 'trailer_angle (find-package "HV_V2V")))
(unless (find-package "HV_V2V::TRAILER_ANGLE")
  (make-package "HV_V2V::TRAILER_ANGLE"))

(in-package "ROS")
;;//! \htmlinclude trailer_angle.msg.html


(defclass hv_v2v::trailer_angle
  :super ros::object
  :slots (_angle ))

(defmethod hv_v2v::trailer_angle
  (:init
   (&key
    ((:angle __angle) 0.0)
    )
   (send-super :init)
   (setq _angle (float __angle))
   self)
  (:angle
   (&optional __angle)
   (if __angle (setq _angle __angle)) _angle)
  (:serialization-length
   ()
   (+
    ;; float32 _angle
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _angle
       (sys::poke _angle (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _angle
     (setq _angle (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get hv_v2v::trailer_angle :md5sum-) "2d11dcdbe5a6f73dd324353dc52315ab")
(setf (get hv_v2v::trailer_angle :datatype-) "hv_v2v/trailer_angle")
(setf (get hv_v2v::trailer_angle :definition-)
      "float32 angle

")



(provide :hv_v2v/trailer_angle "2d11dcdbe5a6f73dd324353dc52315ab")


