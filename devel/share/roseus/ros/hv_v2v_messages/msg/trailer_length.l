;; Auto-generated. Do not edit!


(when (boundp 'hv_v2v_messages::trailer_length)
  (if (not (find-package "HV_V2V_MESSAGES"))
    (make-package "HV_V2V_MESSAGES"))
  (shadow 'trailer_length (find-package "HV_V2V_MESSAGES")))
(unless (find-package "HV_V2V_MESSAGES::TRAILER_LENGTH")
  (make-package "HV_V2V_MESSAGES::TRAILER_LENGTH"))

(in-package "ROS")
;;//! \htmlinclude trailer_length.msg.html


(defclass hv_v2v_messages::trailer_length
  :super ros::object
  :slots (_length ))

(defmethod hv_v2v_messages::trailer_length
  (:init
   (&key
    ((:length __length) 0.0)
    )
   (send-super :init)
   (setq _length (float __length))
   self)
  (:length
   (&optional __length)
   (if __length (setq _length __length)) _length)
  (:serialization-length
   ()
   (+
    ;; float32 _length
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _length
       (sys::poke _length (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _length
     (setq _length (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get hv_v2v_messages::trailer_length :md5sum-) "30516591bd33c945cbc4d8d225f8022b")
(setf (get hv_v2v_messages::trailer_length :datatype-) "hv_v2v_messages/trailer_length")
(setf (get hv_v2v_messages::trailer_length :definition-)
      "float32 length

")



(provide :hv_v2v_messages/trailer_length "30516591bd33c945cbc4d8d225f8022b")


