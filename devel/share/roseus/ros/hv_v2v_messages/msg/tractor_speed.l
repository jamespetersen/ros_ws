;; Auto-generated. Do not edit!


(when (boundp 'hv_v2v_messages::tractor_speed)
  (if (not (find-package "HV_V2V_MESSAGES"))
    (make-package "HV_V2V_MESSAGES"))
  (shadow 'tractor_speed (find-package "HV_V2V_MESSAGES")))
(unless (find-package "HV_V2V_MESSAGES::TRACTOR_SPEED")
  (make-package "HV_V2V_MESSAGES::TRACTOR_SPEED"))

(in-package "ROS")
;;//! \htmlinclude tractor_speed.msg.html


(defclass hv_v2v_messages::tractor_speed
  :super ros::object
  :slots (_speed ))

(defmethod hv_v2v_messages::tractor_speed
  (:init
   (&key
    ((:speed __speed) 0.0)
    )
   (send-super :init)
   (setq _speed (float __speed))
   self)
  (:speed
   (&optional __speed)
   (if __speed (setq _speed __speed)) _speed)
  (:serialization-length
   ()
   (+
    ;; float32 _speed
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _speed
       (sys::poke _speed (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _speed
     (setq _speed (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get hv_v2v_messages::tractor_speed :md5sum-) "ca65bba734a79b4a6707341d829f4d5c")
(setf (get hv_v2v_messages::tractor_speed :datatype-) "hv_v2v_messages/tractor_speed")
(setf (get hv_v2v_messages::tractor_speed :definition-)
      "float32 speed

")



(provide :hv_v2v_messages/tractor_speed "ca65bba734a79b4a6707341d829f4d5c")


