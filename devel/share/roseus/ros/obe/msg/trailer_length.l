;; Auto-generated. Do not edit!


(when (boundp 'obe::trailer_length)
  (if (not (find-package "OBE"))
    (make-package "OBE"))
  (shadow 'trailer_length (find-package "OBE")))
(unless (find-package "OBE::TRAILER_LENGTH")
  (make-package "OBE::TRAILER_LENGTH"))

(in-package "ROS")
;;//! \htmlinclude trailer_length.msg.html


(defclass obe::trailer_length
  :super ros::object
  :slots (_length ))

(defmethod obe::trailer_length
  (:init
   (&key
    ((:length __length) 0.0)
    )
   (send-super :init)
   (setq _length (float __length))
   self)
  (:length
   (&optional __length)
   (if __length (setq _length __length)) _length)
  (:serialization-length
   ()
   (+
    ;; float32 _length
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _length
       (sys::poke _length (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _length
     (setq _length (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get obe::trailer_length :md5sum-) "30516591bd33c945cbc4d8d225f8022b")
(setf (get obe::trailer_length :datatype-) "obe/trailer_length")
(setf (get obe::trailer_length :definition-)
      "float32 length

")



(provide :obe/trailer_length "30516591bd33c945cbc4d8d225f8022b")


