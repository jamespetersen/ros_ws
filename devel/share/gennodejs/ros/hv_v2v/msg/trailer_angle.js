// Auto-generated. Do not edit!

// (in-package hv_v2v.msg)


"use strict";

let _serializer = require('../base_serialize.js');
let _deserializer = require('../base_deserialize.js');
let _finder = require('../find.js');

//-----------------------------------------------------------

class trailer_angle {
  constructor() {
    this.angle = 0.0;
  }

  static serialize(obj, bufferInfo) {
    // Serializes a message object of type trailer_angle
    // Serialize message field [angle]
    bufferInfo = _serializer.float32(obj.angle, bufferInfo);
    return bufferInfo;
  }

  static deserialize(buffer) {
    //deserializes a message object of type trailer_angle
    let tmp;
    let len;
    let data = new trailer_angle();
    // Deserialize message field [angle]
    tmp = _deserializer.float32(buffer);
    data.angle = tmp.data;
    buffer = tmp.buffer;
    return {
      data: data,
      buffer: buffer
    }
  }

  static datatype() {
    // Returns string type for a message object
    return 'hv_v2v/trailer_angle';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '2d11dcdbe5a6f73dd324353dc52315ab';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 angle
    
    `;
  }

};

module.exports = trailer_angle;
