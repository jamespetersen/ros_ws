
"use strict";

let tractor_speed = require('./tractor_speed.js');
let trailer_angle = require('./trailer_angle.js');
let trailer_length = require('./trailer_length.js');

module.exports = {
  tractor_speed: tractor_speed,
  trailer_angle: trailer_angle,
  trailer_length: trailer_length,
};
