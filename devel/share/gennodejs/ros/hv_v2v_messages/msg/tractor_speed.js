// Auto-generated. Do not edit!

// (in-package hv_v2v_messages.msg)


"use strict";

let _serializer = require('../base_serialize.js');
let _deserializer = require('../base_deserialize.js');
let _finder = require('../find.js');

//-----------------------------------------------------------

class tractor_speed {
  constructor() {
    this.speed = 0.0;
  }

  static serialize(obj, bufferInfo) {
    // Serializes a message object of type tractor_speed
    // Serialize message field [speed]
    bufferInfo = _serializer.float32(obj.speed, bufferInfo);
    return bufferInfo;
  }

  static deserialize(buffer) {
    //deserializes a message object of type tractor_speed
    let tmp;
    let len;
    let data = new tractor_speed();
    // Deserialize message field [speed]
    tmp = _deserializer.float32(buffer);
    data.speed = tmp.data;
    buffer = tmp.buffer;
    return {
      data: data,
      buffer: buffer
    }
  }

  static datatype() {
    // Returns string type for a message object
    return 'hv_v2v_messages/tractor_speed';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'ca65bba734a79b4a6707341d829f4d5c';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 speed
    
    `;
  }

};

module.exports = tractor_speed;
