// Auto-generated. Do not edit!

// (in-package obe.msg)


"use strict";

let _serializer = require('../base_serialize.js');
let _deserializer = require('../base_deserialize.js');
let _finder = require('../find.js');

//-----------------------------------------------------------

class trailer_length {
  constructor() {
    this.length = 0.0;
  }

  static serialize(obj, bufferInfo) {
    // Serializes a message object of type trailer_length
    // Serialize message field [length]
    bufferInfo = _serializer.float32(obj.length, bufferInfo);
    return bufferInfo;
  }

  static deserialize(buffer) {
    //deserializes a message object of type trailer_length
    let tmp;
    let len;
    let data = new trailer_length();
    // Deserialize message field [length]
    tmp = _deserializer.float32(buffer);
    data.length = tmp.data;
    buffer = tmp.buffer;
    return {
      data: data,
      buffer: buffer
    }
  }

  static datatype() {
    // Returns string type for a message object
    return 'obe/trailer_length';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '30516591bd33c945cbc4d8d225f8022b';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 length
    
    `;
  }

};

module.exports = trailer_length;
