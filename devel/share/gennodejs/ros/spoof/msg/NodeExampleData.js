// Auto-generated. Do not edit!

// (in-package spoof.msg)


"use strict";

let _serializer = require('../base_serialize.js');
let _deserializer = require('../base_deserialize.js');
let _finder = require('../find.js');

//-----------------------------------------------------------

class NodeExampleData {
  constructor() {
    this.trailer_length = 0.0;
    this.trailer_angle = 0.0;
    this.tractor_speed = 0.0;
  }

  static serialize(obj, bufferInfo) {
    // Serializes a message object of type NodeExampleData
    // Serialize message field [trailer_length]
    bufferInfo = _serializer.float32(obj.trailer_length, bufferInfo);
    // Serialize message field [trailer_angle]
    bufferInfo = _serializer.float32(obj.trailer_angle, bufferInfo);
    // Serialize message field [tractor_speed]
    bufferInfo = _serializer.float32(obj.tractor_speed, bufferInfo);
    return bufferInfo;
  }

  static deserialize(buffer) {
    //deserializes a message object of type NodeExampleData
    let tmp;
    let len;
    let data = new NodeExampleData();
    // Deserialize message field [trailer_length]
    tmp = _deserializer.float32(buffer);
    data.trailer_length = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [trailer_angle]
    tmp = _deserializer.float32(buffer);
    data.trailer_angle = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [tractor_speed]
    tmp = _deserializer.float32(buffer);
    data.tractor_speed = tmp.data;
    buffer = tmp.buffer;
    return {
      data: data,
      buffer: buffer
    }
  }

  static datatype() {
    // Returns string type for a message object
    return 'spoof/NodeExampleData';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '7c755488a7cae0c2d3e46552b9fab888';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 trailer_length
    float32 trailer_angle
    float32 tractor_speed
    
    `;
  }

};

module.exports = NodeExampleData;
