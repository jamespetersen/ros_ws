#include "obe/talker.h"

namespace obe
{

ExampleTalker::ExampleTalker(ros::NodeHandle nh) :
  a_(1), b_(2), c_(3), message_("hello")
{
  // Do this before parameter server, else some of the parameter server values can be overwritten.
//  dynamic_reconfigure::Server<obe::nodeExampleConfig>::CallbackType cb;
//  cb = boost::bind(&ExampleTalker::configCallback, this, _1, _2);
//  dr_srv_.setCallback(cb);

  // Declare variables that can be modified by launch file or command line.
  int rate;

  // Initialize node parameters from launch file or command line. Use a private node handle so that multiple instances
  // of the node can be run simultaneously while using different parameters.
  ros::NodeHandle pnh("~");
  pnh.param("a", a_, a_);
  pnh.param("b", b_, b_);
  pnh.param("c", c_, c_);
  pnh.param("message", message_, message_);
  pnh.param("rate", rate, 1);

  // Create a publisher and name the topic.
  pub_ = nh.advertise<obe::NodeExampleData>("example", 10);

  // Create timer.
  timer_ = nh.createTimer(ros::Duration(1 / rate), &ExampleTalker::timerCallback, this);
}

void ExampleTalker::timerCallback(const ros::TimerEvent& event)
{
  obe::NodeExampleData msg;
  msg.message = message_;
  msg.a = a_;
  msg.b = b_;
  msg.c = c_;

  pub_.publish(msg);
}

//void ExampleTalker::configCallback(obe::nodeExampleConfig& config, uint32_t level)
//{
//  // Set class variables to new values. They should match what is input at the dynamic reconfigure GUI.
//  message_ = config.message.c_str();
//  a_ = config.a;
//  b_ = config.b;
//}

}
