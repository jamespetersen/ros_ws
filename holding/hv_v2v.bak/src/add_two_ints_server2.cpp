#include "ros/ros.h"
#include "beginner_tutorials/AddTwoInts.h"

bool add(beginner_tutorials::AddTwoInts::Request  &req,
         beginner_tutorials::AddTwoInts::Response &res)
{
  res.sum = req.a + req.b;
  ROS_INFO("request2: x=%ld, y=%ld", (long int)req.a, (long int)req.b);
  ROS_INFO("sending back response2: [%ld]", (long int)res.sum);
  return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "add_two_ints_server2");
  ros::NodeHandle n;

  ros::ServiceServer service = n.advertiseService("add_two_ints2", add);
  ROS_INFO("Ready to add two ints2.");
  ros::spin();

  return 0;
}
