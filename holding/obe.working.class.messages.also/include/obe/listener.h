#ifndef OBE_LISTENER_H
#define OBE_LISTENER_H

// ROS includes.
#include "ros/ros.h"
#include "ros/time.h"

// Custom message includes. Auto-generated from msg/ directory.
#include "obe/NodeExampleData.h"

namespace obe
{

class ExampleListener
{
public:
  //! Constructor.
  ExampleListener(ros::NodeHandle nh);

  //! Callback function for subscriber.
  void messageCallback(const obe::NodeExampleData::ConstPtr &msg);

private:
  //! Subscriber to custom message.
  ros::Subscriber sub_;
};

}

#endif // OBE_LISTENER_H
