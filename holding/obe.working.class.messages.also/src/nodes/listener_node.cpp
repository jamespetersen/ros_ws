#include "obe/listener.h"

int main(int argc, char **argv)
{
  // Set up ROS.
  ros::init(argc, argv, "listener");
  ros::NodeHandle nh;

  // Create a new node_example::Talker object.
  obe::ExampleListener node(nh);

  // Let ROS handle all callbacks.
  ros::spin();

  return 0;
} // end main()
