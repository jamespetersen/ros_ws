#include "ros/ros.h"
//#include "std_msgs/String.h"
#include "std_msgs/Float32.h"

#include <sstream>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "spoof");

  ros::NodeHandle n1;
  ros::Publisher pub1 = n1.advertise<std_msgs::Float32>("trailer_length", 1000, true);

  ros::NodeHandle n2;
  ros::Publisher pub2 = n2.advertise<std_msgs::Float32>("trailer_angle", 1000, true);

  //ros::Rate loop_rate(10);
  ros::Rate loop_rate(2);

  float trailer_length=15.24;
  float trailer_angle=0.0;

  int count = 0;
  while (ros::ok())
  {
    std_msgs::Float32 msg_trailer_length;
    std_msgs::Float32 msg_trailer_angle;

    msg_trailer_length.data = trailer_length;
    trailer_length += 0.01;
    ROS_INFO("trailer_length = %f", msg_trailer_length.data);
    pub1.publish(msg_trailer_length);

    msg_trailer_angle.data = trailer_angle;
    trailer_angle += 0.1;
    ROS_INFO("trailer_angle = %f", msg_trailer_angle.data);
    pub2.publish(msg_trailer_angle);


    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }


  return 0;
}
